'use strict';

require('./styles/gallery.scss');

export default angular
  .module('gallery', [])
  .controller('galleryController', require('./gallery.controller.js'));
