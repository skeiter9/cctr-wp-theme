'use strict';

require('./styles/location.scss');

export default angular
  .module('location', [])
  .controller('locationController', require('./location.controller.js'));
