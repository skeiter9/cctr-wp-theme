'use strict';

require('./styles/services.scss');

export default angular
  .module('services', [])
  .controller('servicesController', require('./services.controller.js'));
