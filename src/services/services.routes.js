'use strict';

export function routes(stateProvider) {

  let stateName = 'services';

  stateProvider

    .state(stateName, {
      parent: 'layout',
      url: '/services',
      resolve: {
        boot: ['layoutFactory', (lF) => lF.stateLoad(stateName)]
      },
      views: {
        'content': {
          controllerAs: 'home',
          templateProvider: ['layoutFactory', (lF) => lF.ui.viewTemplate],
          controllerProvider: ['layoutFactory', (lF) => lF.ui.controllerName]
        }
      }
    });

};
