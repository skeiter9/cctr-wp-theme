'use strict';

require('./styles/events.scss');

export default angular
  .module('events', [])
  .controller('eventsController', require('./events.controller.js'));
