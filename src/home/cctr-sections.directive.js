'use strict';

module.exports = cctrSectionFn;

function cctrSectionFn() {
  return {
    bindToController: true,
    controllerAs: 'cctrSections',
    controller: angular.noop,
    link: link,
    restrict: 'A',
    scope: {},
    template: require('./templates/cctr-sections.jade')()
  }

  function link(scope, element, attrs, cctrSplash) {
    //element[0].style.height
  }
}
cctrSectionFn.$inject = [];
