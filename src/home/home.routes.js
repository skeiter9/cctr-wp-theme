'use strict';

export function routes(stateProvider) {

  stateProvider

    .state('home', {
      parent: 'layout',
      url: '/',
      resolve: {
        boot: ['layoutFactory', (lF) => lF.stateLoad('home')]
      },
      views: {
        'content': {
          controllerAs: 'home',
          templateProvider: ['layoutFactory', (lF) => lF.ui.viewTemplate],
          controllerProvider: ['layoutFactory', (lF) => lF.ui.controllerName]
        }
      }
    });

};
