'use strict';

require('./styles/home.scss');

module.exports = angular
  .module('cctrHome', [])
  .directive('cctrSections', require('./cctr-sections.directive.js'))
  .controller('homeController', require('./home.controller.js'));
