'use strict';

require('./styles/layout-web.scss');

module.exports = angular
  .module('layoutWeb', [
    require('./layout').name
  ])
  .directive('mdList', ['$$rAF', 'yeTweenLite', mdListDirectiveFn]);

function mdListDirectiveFn($$rAF, yeTweenLite) {

  return {
    link: link
  };

  function link(scope, element, attrs) {
    $$rAF(function() {
      var menuItems = element[0].querySelectorAll('md-list-item');

      yeTweenLite.fromTo(menuItems, 1, {opacity: 0, y: '20px'},
        {delay: 6, opacity: 1, y: 0});
    });

  }

}
