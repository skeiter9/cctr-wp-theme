'use strict';

export default function layoutFactory($q, $oc, $tr, $trPL, yeTweenLite,
  $state) {

  let Layout = class LayoutP {

    constructor() {

      this.data = {
        mode: localStorage.getItem('cctrMode') === 'app' ? 'app' : 'web',
        dataUser: {},
      };

      localStorage.removeItem('cctrMode');

      this.ui = {
        smScreen: 600,
        mdScreen: 960,
        lgScreen: 1200,
        xlgScreen: 1600,
        assetsLoaded: [],
        content: {
          'classContainer': '',
        },
        appbar: {
          title: 'consama'
        },
        sidenav: {
          left: {},
          right: {
            type: '',
            theme: 'default',
            toolbar: {
              title: 'sidenav right title',
              icons: {
                left: {},
                right: []
              }
            },
            content: {
              html: ''
            }
          }
        }
      };

    }

    stateLoadPre(resolveState) {

      if (
        !angular.isString(resolveState) &&
        !angular.isFunction(resolveState)
      ) return $q((resolve, r) => r(`pass a function or name for the view`));

      let res = angular.isString(resolveState) ?
        this.requiForView(resolveState) :
        angular.isFunction(resolveState) ? resolveState() :
        {};

      return $q.when(res);

    }

    requiForView(viewName) {

      let res = {
        toLoad: [
          this.getUrlFile('app', '.css'),
          this.getUrlFile('layout-' + this.data.mode, '.css'),
          'layout.translate'
        ]
      };

      res.view = viewName;

      if (viewName === 'home') res.toLoad.push(
        this.getUrlFile(viewName),
        this.getUrlFile(viewName, '.css'),
        `${viewName}.translate`
      );
      else if (viewName === 'location') res.toLoad.push(
        this.getUrlFile(viewName),
        this.getUrlFile(viewName, '.css'),
        `${viewName}.translate`
      );
      else if (viewName === 'events') res.toLoad.push(
        this.getUrlFile(viewName),
        this.getUrlFile(viewName, '.css'),
        `${viewName}.translate`
      );
      else if (viewName === 'services') res.toLoad.push(
        this.getUrlFile(viewName),
        this.getUrlFile(viewName, '.css'),
        `${viewName}.translate`
      );
      else if (viewName === 'gallery') res.toLoad.push(
        this.getUrlFile(viewName),
        this.getUrlFile(viewName, '.css'),
        `${viewName}.translate`
      );
      else if (viewName === 'contact') res.toLoad.push(
        this.getUrlFile(viewName),
        this.getUrlFile(viewName, '.css'),
        `${viewName}.translate`
      );

      return res;

    }

    stateLoad(resolveState) {

      return this.stateLoadPre(resolveState)

        .then((res) => this.loadFilesSetview(res))

        .then((data) => this.postLoadState(data));
    }

    postLoadState(data) {

      return $tr.refresh()
        .then(() => this.clearInitialLoader())
        .catch(() => {
          console.log('can\'t refresh translates');
          return this.clearInitialLoader();
        });

    }

    isLoaded(src) {
      return this.ui.assetsLoaded.indexOf(src) === -1 ? false : true;
    }

    loadFilesSetview(res) {

      this.ui.viewTemplate = this.appTemplates(res.view);
      this.ui.stateName = res.view;

      this.ui.controllerName = `${res.view}Controller`;

      let files = [];

      for (let i = 0; i < res.toLoad.length; i++) ((file) => {

        if (
          !this.isLoaded(file) &&
          !/\.translate/.test(file)
        ) files.push(file);
        else if (
          !this.isLoaded(file) &&
          /\.translate/.test(file)
        ) $trPL.addPart(file.slice(0, -10));

      })(res.toLoad[i]);

      return $oc.load(files)
        .then((data) => {
          for (let i = 0; i < files.length; i++) {
            this.ui.assetsLoaded.push(files[i]);
          }
          return data;
        })
        .catch((e) => e);

    }

    appTemplates(stateName = 'nothing') {
      let t = stateName;
      if (
        stateName === 'home'
      ) t = require('../../home/templates/home.jade')();
      else if (
        stateName === 'location'
      ) t = require('../../location/templates/location.jade')();
      else if (
        stateName === 'gallery'
      ) t = require('../../gallery/templates/gallery.jade')();
      else if (
        stateName === 'events'
      ) t = require('../../events/templates/events.jade')();
      else if (
        stateName === 'services'
      ) t = require('../../services/templates/services.jade')();
      else if (
        stateName === 'contact'
      ) t = require('../../contact/templates/contact.jade')();
      return t;
    }

    clearInitialLoader() {
      return $q((resolve, reject) => {
        let loader = document.getElementById('loader');
        if (loader !== null) {
          TweenLite.to(loader, 1, {delay: 1, opacity: 0, onComplete: () => {
            loader.remove();
          }});
          resolve();
        }else resolve();
      });
    }

    getUrlFile(name, ext = '.js') {
      return (__PRODUCTION__ ?
        `wp-content/themes/cctr-wp-theme/static/build/` :
        `http://localhost:8090/build/`) + `${name}.bundle${ext}`;
    }

    goTo(stateName) {
      return $state.go(stateName);
    }

  };

  return new Layout();

}
