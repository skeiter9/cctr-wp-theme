'use strict';

module.exports = LayoutController;

function LayoutController(lF) {

  let layout = this;

  layout.vm = lF;

  //layout.showAppbar = false;

  layout.vm.ui.menu = [
    {name: 'home', icon: 'home'},
    //{name: 'us', icon: 'home'},
    {name: 'events', icon: 'home'},
    {name: 'location', icon: 'home'},
    {name: 'services', icon: 'home'},
    {name: 'gallery', icon: 'home'},
    //{name: 'blog', icon: 'home'},
    //{name: 'book', icon: 'home'},
    {name: 'contact', icon: 'home'}
  ];

  let logo = require('file?name=../../static/images/[name]XX.[ext]!' +
    'img?minimize&optimizationLevel=7!./images/logo-cctr.png');

  //layout.vm.ui.logo = logo;

  layout.vm.ui.host = `wp-content/themes/cctr-wp-theme/static/`;

}

LayoutController.$inject = ['layoutFactory'];
