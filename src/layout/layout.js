'use strict';

require('angular-translate');
require('angular-translate-loader-partial');
require('angular-dynamic-locale');

import googleMaps from '../utils/google-maps.js';
import capitalizeFilerAM from '../utils/capitalize.filter';
import cctrSplash from '../utils/cctr-splash.directive.js';

import layoutFactoryFn from './logic/layout-provider.js';

export default angular
  .module('appLayout', [
    'pascalprecht.translate',
    'tmh.dynamicLocale',
    require('angular-sanitize'),
    require('angular-animate'),
    require('../utils/gsap.factory.js').name,
    capitalizeFilerAM.name,
    cctrSplash.name,
    googleMaps.name
  ])
  .config(['tmhDynamicLocaleProvider', '$translateProvider', config])
  .provider('layoutFactory', layoutFactoryProviderFn)
  .directive('yeCardPhoto', ['$window', ($w) => {
    return (s, elem, attrs) => {

      let resize = (e) => {
        elem[0].style.height = elem[0].parentNode
          .querySelector('.ye-card__content').offsetHeight + 'px';
        elem[0].style.backgroundImage = `url('${attrs.cover}')`;
      };

      resize();
      $w.addEventListener('resize' , resize);

    };
  }])
  .directive('yeCardCover', ['$window', ($w) => {
    return (s, elem, attrs) => {

      let resize = (e) => {
        elem[0].style.height = (elem[0].parentNode
          .offsetWidth * (9 / 16)) + 'px';
        elem[0].style.backgroundImage = `url('${attrs.cover}')`;
      };

      resize();
      $w.addEventListener('resize' , resize);

    };
  }])
  .controller('LayoutController', require('./layout.controller.js'));

function config(tmhDynamicLocaleProvider, $translateProvider) {

  tmhDynamicLocaleProvider
    .localeLocationPattern('static/i18n/ngLocale/angular-locale_{{locale}}.js');

  $translateProvider
    .useSanitizeValueStrategy('escaped')
    .useLoader('$translatePartialLoader', {
      urlTemplate: '/settings/translate?part={part}&lang={lang}'
    });

}

function layoutFactoryProviderFn() {

  this.$get = ['$q', '$ocLazyLoad', '$translate', '$translatePartialLoader',
    'yeTweenLite', '$state', layoutFactoryFn];

}
