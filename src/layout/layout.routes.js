'use strict';

export function routes(stateProvider) {

  stateProvider

    .state('layout', {
      abstract: true,
      url: '^',
      template: require('./templates/layout.jade')(),
      controller: 'LayoutController',
      controllerAs: 'layoutVm'
    });

};
