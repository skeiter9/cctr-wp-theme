'use strict';

import loadScript from './load-script.js';

export default (w, d) => {

  let injector = angular.injector(['ng']);
  let $q = injector.get('$q');

  let Gradify = require('gradify');
  let loader = d.getElementById('loader');
  let bg = d.createElement('img');

  bg.src = (__PRODUCTION__ ?
    `wp-content/themes/cctr-wp-theme/static` :
    `http://cctr.io/wp-content/themes/cctr-wp-theme/static`) +
    '/images/splash.jpg';

  bg.addEventListener('load', function() {
    let grad = new Gradify(bg, 'loader__bg');
    loader.classList.add('loader__bgc');
  });

  let lS = w.localStorage;

  w.WebFontConfig = {
    google: {families: ['Julius+Sans+One::latin', 'Laila:400,500:latin']}
  };

  let cctrMode = lS.getItem('cctrMode');
  if (
    cctrMode === null &&
    ('ontouchstart' in w || 'onmsgesturechange' in w)
  ) lS.setItem('cctrMode', 'app');
  else if (
    cctrMode === null && w.innerWidth < 768
  ) lS.setItem('cctrMode', 'app');
  else if (
    cctrMode === null &&
    w.innerWidth >= 768
  ) lS.setItem('cctrMode', 'web');
  else if (
    cctrMode === null &&
    cctrMode !== 'app'
  ) lS.setItem('cctrMode', 'web');

  let layoutModeFile = (__PRODUCTION__ ?
    'wp-content/themes/cctr-wp-theme/static/build/layout-' :
    'http://localhost:8090/build/layout-') +
    lS.getItem('cctrMode') + '.bundle.js';

  return $q.all([
    loadScript('http://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js'),
    loadScript(layoutModeFile)
  ]);

};
