'use strict';

export default (url) => {

  let injector = angular.injector(['ng']);
  let $q = injector.get('$q');

  return $q((resolve, reject) => {

    let script = document.createElement('script');
    let body = document.body;
    let scriptsLoaded = body.getElementsByTagName('script');
    let lastScript = scriptsLoaded[scriptsLoaded.length - 1];
    let removed = false;

    script.type = 'text/javascript';
    script.async = false;
    script.src = url;

    script.onload = () => resolve(url);
    script.onerror = (e) => reject(e);

    lastScript.parentNode.insertBefore(script, lastScript);

  });

};
