'use strict';

import * as layoutRouter from '../layout/layout.routes.js';
import * as homeRouter from '../home/home.routes.js';
import * as eventsRouter from '../events/events.routes.js';
import * as contactRouter from '../contact/contact.routes.js';
import * as galleryRouter from '../gallery/gallery.routes.js';
import * as servicesRouter from '../services/services.routes.js';
import * as locationRouter from '../location/location.routes.js';

import layoutAM from '../layout/layout.js';

export default angular

  .module('cctrRoutes', [
    'oc.lazyLoad',
    require('angular-ui-router'),
    layoutAM.name
  ])
  .config(['$stateProvider', '$locationProvider', ($sP, $lP) => {

    $lP.html5Mode({
      enabled: true,
      requiBase: true
    });

    layoutRouter.routes($sP);
    homeRouter.routes($sP);
    eventsRouter.routes($sP);
    contactRouter.routes($sP);
    galleryRouter.routes($sP);
    servicesRouter.routes($sP);
    locationRouter.routes($sP);
  }]);
