'use strict';

require('./styles/contact.scss');

export default angular
  .module('contact', [])
  .controller('contactController', require('./contact.controller.js'));
