'use strict';

export default angular
  .module('components.capitalize', [])
  .filter('capitalize', ['$log', capitalizeFilterFn])
  .filter('capitalizeAll', ['capitalizeFilter', capitalizeAllFilterFn]);

function capitalizeFilterFn($log) {

  return function capitalizeWord(word) {

    if (angular.isNumber(word) || angular.isDate(word)) return word;
    if (angular.isUndefined(word)) return $log.warn('the input word to ' +
      'capitalize is undefined');

    var wordResult = word.toLowerCase();
    return wordResult.substring(0, 1).toUpperCase() + wordResult.substring(1);
  };

}

function capitalizeAllFilterFn(capitalizeFilter) {
  return function(sentence) {
    var auxSenetence = sentence.split(' ');
    var res = '';
    angular.forEach(auxSenetence,  function(part) {
      res += capitalizeFilter(part) + ' ';
    });
    return res;
  };
};
