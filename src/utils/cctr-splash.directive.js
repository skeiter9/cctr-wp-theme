'use strict';

export default angular
  .module('cctrSplash', [])
  .animation('.appbar', ['$animateCss', ($animateCss) => {
    return {
      enter(element, doneFn) {
        // this will trigger `.slide.ng-enter` and `.slide.ng-enter-active`.
        let runner = $animateCss(element, {
          event: 'enter',
          //structural: true,
          addClass: 'my-class',
          duration: .3,
          //stagger: .45,
          //delay: .45,
          from: {opacity: 0},
          to: {opacity: 1}
        })
        .start()
        .finally(() => {
          console.log('done enter');
          doneFn();
        });
      },
      leave(element, doneFn) {
        // this will trigger `.slide.ng-leave` and `.slide.ng-leave-active`.
        let runner = $animateCss(element, {
          event: 'leave',
          //structural: true,
          addClass: 'my-class',
          duration: .45,
          //stagger: .3,
          from: {opacity: 1},
          to: {opacity: 0}
        })
        .start()
        .finally(() => {
          console.log('done leave');
          doneFn();
        });
      },
    };
  }])
  .directive('cctrSplash', ['layoutFactory', 'yeTweenLite', '$q', '$window',
    'yeVerge', '$animateCss', '$animate', '$timeout', '$sce', '$state',
    cctrSplashFn]);

function cctrSplashFn(lF, yeTL, $q, $w, yeV, $aCss, $a, $t, $sce, $state) {

  return {
    bindToController: true,
    controllerAs: 'cctrSplash',
    controller: angular.noop,
    compile(tE) {

      return {
        pre(s, elem, attrs, ctrl) {
          //
          ctrl.bg = lF.data.mode === 'web' ? 'video' : 'img';

          ctrl.videoUrl = $sce.trustAsResourceUrl('https://www.dropbox.com/' +
            's/9dkdeaz0sbiy88r/cctr.mp4?dl=1');

        },
        post(s, elem, attrs, ctrl) {
          ctrl.initialize = false;
          //let bgVideo = elem[0].querySelector('.cctr-splash__bg__video');
          //let menuItems = elem[0].querySelector('.cctr-splash__menu');
          //let logo = elem[0].querySelector('.cctr-splash__content__logo');
          let bg = elem[0].querySelector('.cctr-splash__bg');
          let logo = elem[0].querySelector('.logo');
          let contentWrap = elem[0].querySelector('.cctr-splash__menu-toggle');
          let content = elem[0].querySelector('.cctr-splash__menu');
          let contentFooter = elem[0].querySelector('.cctr-splash__footer');
          let appbar = elem[0].querySelector('.appbar');

          contentWrap.style.opacity = 0;
          content.style.opacity = 0;
          contentFooter.style.opacity = 0;
          appbar.style.opacity = 0;

          if (ctrl.init) return;
          ctrl.init = true;

          init();

          function init() {
            $t(() => {
              $t(() => {
                resize();
                s.$on('$stateChangeSuccess', (evt, tS, tP, fS, fP) => {
                  resize(evt, tS.name, fS.name);
                });

                $w.addEventListener('resize', resize);

                ctrl.openMenu = (e) => {
                  let animContentWrap = $aCss(angular.element(contentWrap), {
                    from: {
                      opacity: 1
                    },
                    to: {
                      opacity: 1
                    },
                    addClass: 'open',
                    duration: 0.3
                  }).start();
                };

                ctrl.goTo = (e, stateName) => {
                  $aCss(angular.element(contentWrap), {
                    removeClass: 'open',
                    duration: 0.3
                  }).start()
                    .then(() => {
                      return $state.go(stateName);
                    });
                };
              }, 0);
            }, 0);
          }

          function resize(evt, stateName = $state.current.name, fSName = '') {

            ctrl.appbarTitle = stateName;
            if (
              stateName === 'home' && fSName !== ''
            ) contentWrap.classList.remove('sidenav');
            else if (
              stateName !== 'home' && fSName === 'home'
            ) contentWrap.style.opacity = 0;

            let animSplasScreen = $aCss(elem, {
              from: {
                height: stateName === 'home' ?
                //fSName === '' ? `${yeV.viewportH()}px` :
                '48px' : `${yeV.viewportH()}px`
              },
              to: {
                height: stateName === 'home' ? `${yeV.viewportH()}px` : '48px'
              },
              duration: 0.45
            });

            let animContent = $aCss(angular.element(content), {
              from: {
                opacity: 0
              },
              to: {
                opacity: 1
              },
              duration: 0.45
            });

            let animContentFooter = $aCss(angular.element(contentFooter), {
              to: {
                opacity: 1
              },
              duration: 0.3
            });

            let animAppbar = $aCss(angular.element(appbar), {
              to: {
                opacity: stateName === 'home' ? 0 : 1
              },
              easing: 'ease-out',
              duration: stateName === 'home' ? 0.3 : 0.75
            });

            let animContentWrap = $aCss(angular.element(contentWrap), {
              from: {
                opacity: 0
              },
              to: {
                opacity: stateName !== 'home' &&
                  (fSName === '' || fSName === 'home') ? 0 : 1
              },
              addClass: stateName === 'home' ? '' : 'sidenav',
              removeClass: stateName === 'home' ? 'sidenav' : '',
              duration: 0.3
            });

            let animBgLogoFn = (bL, isLogo) => $aCss(angular.element(bL), {
              from: {
                height: stateName === 'home' && fSName !== '' ?
                  yeV.viewportH() + 'px' : 'auto'
              },
              to: {
                height: stateName === 'home' ?
                  isLogo ? 'auto' :
                  yeV.viewportH() + 'px' :
                  (yeV.viewportW() >= 304 ?
                    304 * (9 / 16) :
                    (yeV.viewportW() - 56) * (9 / 16)) + 'px'
              },
              duration: 0.45
            });
            let animBg = animBgLogoFn(bg);
            let animLogo = animBgLogoFn(logo, true);

            return $q.all([
              animSplasScreen.start(),
              animContent.start(),
              animContentWrap.start(),
              animBg.start(),
              animLogo.start(),
              stateName === 'home' ? $q.when() : animContentFooter.start(),
              stateName === 'home' || fSName !== '' ?
                animAppbar.start() : $q.when()
            ])
              .then(() => {
                return $q.all([
                  stateName === 'home' || fSName !== '' ?
                    $q.when() : animAppbar.start(),
                  stateName === 'home' ? animContentFooter.start() : $q.when()
                ]);
              })
              .then(() => {
                ctrl.initialize = true;
                return;
              });

          }

        }
      };

    },
    restrict: 'A',
    scope: {
      menu: '=',
      logo: '@'
    },
    template: require('./templates/cctr-splash.jade')()
  };
}
