'use strict';

export default angular
  .module('gsap', [])
  .factory('yeTweenLite', ['$q', yeTweenLiteFactoryFn]);

function yeTweenLiteFactoryFn($q) {

  return {
    fromTo: fromTo,
    to: to
  };

  function fromTo(node, time, from_, to_) {
    var deferred = $q.defer();
    to_.onComplete = function() {
      deferred.resolve();
    };
    TweenLite.fromTo(node, time, from_, to_);
    return deferred.promise;
  }

  function to(node, time, to_) {
    var deferred = $q.defer();
    to_.onComplete = function() {
      deferred.resolve();
    };
    TweenLite.to(node, time, to_);
    return deferred.promise;
  };
}
