'use strict';

module.exports = angular
  .module('appUtilsVerge', [])
  .factory('yeVerge', getVerge);

function getVerge() {
  return require('verge');
}

getVerge.$inject = [];
