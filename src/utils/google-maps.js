'use strict';

export default angular
  .module('gmap', [
    require('./platform-environment.factory').name,
    require('./verge.factory').name
  ])
  .value('google', window.google)
  .directive('gmap', ['google', 'yeVerge', '$window', '$$rAF', '$log',
    '$animate', '$document', '$q', '$timeout', gmapDirectiveFn]);

function gmapDirectiveFn(google, yeVerge, $w, $$rAF, $log, $a,
  $d, $q, $t) {

  return {
    scope: {},
    template: require('./templates/gmap.jade')(),
    controller: angular.noop,
    link(s, elem, attrs, ctrl) {

      s.gmap = {showMap: false};

      let centerCoordinates = new google.maps.LatLng(-6.48454, -76.3698509);
      let mapPoster = elem[0].querySelector('.gmap__map__poster');
      let mapWrapper = elem[0].querySelector('.gmap__map__wrapper');
      let hMap = yeVerge.viewportH() * (9 / 16);

      mapPoster.style.height = hMap + 'px';
      mapWrapper.style.height = hMap + 'px';

      if (angular.isUndefined(google)) {
        mapPoster.classList.add('fail');
        let icon = $d[0].createElement('span');
        icon.classList.add('mdi', 'mdi-alert-circle');
        mapPoster.appendChild(icon);
        return false;
      }

      let map = new google.maps.Map(
        mapWrapper, {
        center: centerCoordinates,
        zoom: 15,
        draggable: false,
        scrollwheel: false,
        zoomControl: true,
        disableDefaultUI: true
      });

      let marker = new google.maps.Marker({
        position: centerCoordinates,
        animation: google.maps.Animation.DROP,
        map: map,
        place: google.maps.Place,
        //[angular.isDefined(attrs.icon) ? 'icon' : 'nn']: pIcon(attrs.icon),
        draggable: false
      });

      let initMap = google.maps.event.addListenerOnce(map, 'idle', () => {
        s.$apply(() => s.gmap.showMap = true);
        //$a.addClass(angular.element(mapPoster), 'ye-fade');
        //$a.addClass(angular.element(mapWrapper), 'ye-appear');
      });

      s.$on('$destroy', function() {

        google.maps.event.removeListener(initMap);

      });

    }
  };

}
