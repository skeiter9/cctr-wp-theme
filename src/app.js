'use strict';

require('boot/styles/app.scss');

import load from './boot/load.js';
import routes from './boot/routes.js';

((w, d) => {

  load(w, d)
    .then((urls) => {
      //
    })
    .catch((e) => w.console.warn(e));

  w.addEventListener('load', (e) => {

    let layoutMod = w.localStorage.getItem('cctrMode') === 'app' ?
      'layoutApp' : 'layoutWeb';

    return angular.bootstrap(d, [layoutMod, routes.name], {strictDi: true});

  });

})(window, document);
