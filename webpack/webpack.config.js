var path = require('path');
var Webpack = require('webpack');

var ExtractTextPlugin = require('extract-text-webpack-plugin');

var srcPath = path.join(__dirname, '..', 'src');
var buildPath = path.join(__dirname, '..', 'static', 'build');

var npmPath = path.join(__dirname, '..', 'node_modules');
var bowerPath = path.join(__dirname, '..', 'bower_components');

var port = process.env.PORT || 8090;
var DEV = JSON.parse(process.env.BUILD_DEV || 'true');
var DEPLOY = JSON.parse(process.env.BUILD_DEPLOY || 'false');

var config = {
  addVendor: function(name, path_, isParse) {
    this.resolve.alias[name] = path_;
    if (!isParse) this.module.noParse.push(new RegExp(path_));
  },
  context: srcPath,
  debug: true,
  devtool: 'cheap-source-map',
  entry: {
    app: [
      'webpack/hot/dev-server',
      'webpack-dev-server/client?http://localhost:' + port,
      './app'
    ],
    'layout-app': './layout/layout-app.js',
    'layout-web': './layout/layout-web.js',
    home: './home/home.js',
    location: './location/location.js',
    events: './events/events.js',
    services: './services/services.js',
    gallery: './gallery/gallery.js',
    contact: './contact/contact.js',
    vendor: [
      'TweenLite',
      'CSSPlugin',
      'angular/angular.min',
      'ocLazyLoad',
      'angular-translate',
      'angular-translate-loader-partial',
      'angular-dynamic-locale'
    ]
  },
  output: {
    filename: '[name].bundle.js',
    path: buildPath,
    publicPath: 'http://localhost:8090/build/',
    chunkFilename: '[id]-chunk.js',
    pathinfo: true
  },
  module: {
    noParse: [],
    preLoaders: [
      {test: /\.js$/, loaders: ['jscs'],
        exclude: /node_modules|bower_components/},
      {test: /\.js$/, loaders: ['source-map']}
    ],
    loaders: [
      {test: /\.js$/, loader: 'babel', exclude: /node_modules|bower_components/},
      {test: /\.css$/, loader: 'style!css'},
      //{test: /\.styl$/, loader: 'style!css?sourceMap!stylus?sourceMap'},
      {test: /\.scss$/, loader: ExtractTextPlugin.extract('style',
        'css?sourceMap!sass?sourceMap&sourceComments&includePaths[]=' +
        bowerPath)},
      {test: /\.jade$/, loader: 'jade'},
      //{test: /\.(png|jpg|jpeg)$/,
      //  loader: 'url?limit=20000&name=assets/[name].[ext]!img?' +
      //    'minimize&progressive=true'},
      {test: /\.(woff|woff2)$/, loader: 'url?limit=100000&name=[name].[ext]'}
    ]
  },
  plugins: [
    new Webpack.DefinePlugin({
      __DEVELOPMENT__: !!DEV,
      __PRODUCTION__: !!DEPLOY
    }),
    new Webpack.ProvidePlugin({
      //angular: 'exports?window.angular!angular/angular.min'
    }),
    new Webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      filename: 'vendor.bundle.js',
      minSize: 20
    }),
    new ExtractTextPlugin('[name].bundle.css')
  ],
  resolve: {
    root: srcPath,
    alias: [],
    extensions: [
      '',
      '.js',
      '.jade',
      '.scss'
    ],
    modulesDirectories: ['node_modules'],
    fallback: [path.join(__dirname, 'node_modules')]
  },
  resolveLoader: {
    modulesDirectories: [path.join(__dirname, 'node_modules')]
  }
};

if (DEV) {
  config.plugins.unshift(new Webpack.HotModuleReplacementPlugin());
}

if (DEPLOY) {

  config.entry.app = './app';
  config.debug = false;
  config.devtool = 'source-map';
  config.output.pathinfo = false;

  //config.plugins.push(new Webpack.optimize.UglifyJsPlugin());
  config.plugins.push(new Webpack.optimize
    .MinChunkSizePlugin({minChunkSize: 40}));

}

//pre load
config.addVendor('gradify',
  bowerPath + '/gradify/grad.js');

//load in vendro
config.addVendor('angular-material',
  bowerPath + '/angular-material/angular-material.min.js');

config.addVendor('angular-material-css',
  bowerPath + '/angular-material/angular-material.min.css', true);

config.addVendor('angular-ui-router',
  bowerPath + '/angular-ui-router/release/angular-ui-router.min.js');

config.addVendor('ocLazyLoad',
  bowerPath + '/oclazyload/dist/ocLazyLoad.min.js');

config.addVendor('TweenLite',
  bowerPath + '/gsap/src/uncompressed/TweenLite', true);

config.addVendor('CSSPlugin',
  bowerPath + '/gsap/src/uncompressed/plugins/CSSPlugin', true);

config.addVendor('angular-dynamic-locale',
  bowerPath + '/angular-dynamic-locale/tmhDynamicLocale.min');

config.addVendor('angular-translate',
  bowerPath + '/angular-translate/angular-translate.min');

config.addVendor('angular-translate-loader-partial',
  bowerPath + '/angular-translate-loader-partial/' +
  'angular-translate-loader-partial.min');

config.addVendor('verge',
  bowerPath + '/verge');

module.exports = config;
/*

var path = require('path');
var Webpack = require('webpack');

var ExtractTextPlugin = require('extract-text-webpack-plugin');

var srcPath = path.join(__dirname, '..', 'src');
var buildPath = path.join(__dirname, '..', 'static', 'build');

var npmPath = path.join(__dirname, '..', 'node_modules');
var bowerPath = path.join(__dirname, '..', 'bower_components');

var DEV = JSON.parse(process.env.BUILD_DEV || 'true');
var DEPLOY = JSON.parse(process.env.BUILD_DEPLOY || 'false');

var config = {
  addVendor: function(name, path_, parse) {
    this.resolve.alias[name] = path_;
    if (!parse) this.module.noParse.push(new RegExp(path_))
  },
  context: srcPath,
  debug: true,
  devtool: 'cheap-source-map',
  entry: {
    app: [
      'webpack/hot/dev-server',
      'webpack-dev-server/client?http://localhost:' + (process.env.WDS || 8090),
      './app'
    ],
    'layout-app': './layout/layout-app.js',
    'layout-web': './layout/layout-web.js',
    home: './home/home.js',
    vendor: [
      'TweenLite',
      'CSSPlugin',
      'angular/angular.min',
      'ocLazyLoad',
      'angular-translate',
      'angular-translate-loader-partial',
      'angular-dynamic-locale'
    ]
  },
  output: {
    filename: '[name].bundle.js',
    path: buildPath,
    publicPath: 'http://localhost:8090/build/',
    chunkFilename: '[id]-chunk.js',
    pathinfo: true
  },
  module: {
    noParse: [],
    preLoaders: [
      {test: /\.js$/, loaders: ['jscs'],
        exclude: /node_modules|bower_components/},
      {test: /\.js$/, loaders: ['source-map']}
    ],
    loaders: [
      {test: /\.css$/, loader: 'style!css'},
      {test: /\.styl$/, loader: 'style!css?sourceMap!stylus?sourceMap'},
      {test: /\.scss$/, loader: ExtractTextPlugin.extract('style',
        'css?sourceMap!sass?sourceMap&sourceComments&includePaths[]=' +
        bowerPath)},
      {test: /\.jade$/, loader: 'jade'},
      {test: /\.twig$/, loader: 'html!webpack-twig'},
      //{test: /\.(png|jpg|jpeg)$/,
      //    loader: 'url?limit=10000&name=images/[name].[ext]!img?' +
      //      'minimize&progressive=true'},
      {test: /\.(woff|woff2)$/, loader: 'url?limit=100000&name=[name].[ext]'}
    ]
  },
  plugins: [
    new Webpack.DefinePlugin({
      __DEVELOPMENT__: DEV,
      __PRODUCTION__: DEPLOY
    }),
    new Webpack.ProvidePlugin({
      //angular: 'exports?window.angular!angular/angular.min'
    }),
    new Webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      filename: 'vendor.bundle.js',
      minSize: 50
    }),
    new ExtractTextPlugin('[name].css')
  ],
  resolve: {
    root: [srcPath],
    alias: [],
    extensions: [
      '',
      '.js',
      '.jade',
      '.scss',
      '.styl'
    ],
    modulesDirectories: ['node_modules'],
    fallback: [path.join(__dirname, 'node_modules')]
  },
  resolveLoader: {
    modulesDirectories: [path.join(__dirname, 'node_modules')]
  }
};

if (DEV) {
  config.plugins.unshift(new Webpack.HotModuleReplacementPlugin());
}

if (DEPLOY) {

  config.entry.app = './app';
  config.debug = false;
  config.devtool = 'source-map';
  config.output.pathinfo = false;

  config.plugins.push(new Webpack.optimize.UglifyJsPlugin());
  config.plugins.push(new Webpack.optimize.MinChunkSizePlugin(100));

}

module.exports = config;
*/
